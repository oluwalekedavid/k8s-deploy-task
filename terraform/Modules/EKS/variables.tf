
variable "vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "public_subnet_ids" {
  description = "List of public subnet IDs"
  type        = list(string)
}

variable "private_subnet_ids" {
  description = "List of private subnet IDs"
  type        = list(string)
}

variable "cluster_name" {
  description = "The name to give to this environment. Will be used for naming various resources."
  default = "Quandoo-Cluster"
}



variable "k8s_desired_size" {
  default = 2
}

variable "k8s_min_size" {
  default = 2
}

variable "k8s_max_size"{
default = 4
}

variable "instance_type" {
  default = "t2.small"
}

variable "ec2_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "ec2_key_name" {
  default = "worker-node-key"
}

variable "worker_node_instance_types" {
  default = ["t2.small"]
}

variable "vpc_security_group_ids" {
  description = "This is security group that would be used by worker nodes"
  type        = list(string)
}