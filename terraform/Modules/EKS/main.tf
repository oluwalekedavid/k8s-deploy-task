#
# Control Plane resources
#

resource "aws_iam_role" "quandoo_cluster_policy" {
  name = "Quandoo-eks-master"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

#attaching pre-defined policy  named "AmazonEKSClusterPolicy" to the role that was created named "master", which grants permissions necessary for managing Amazon EKS clusters.
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.quandoo_cluster_policy.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.quandoo_cluster_policy.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.quandoo_cluster_policy.name
}


#
# Worker Node resources
#


#This specific role is intended for a Kubernetes (K8s) node group in your AWS infrastructure.
resource "aws_iam_role" "worker_node_policy" {
  name = "quandoo-eks-node-group-policy"

  assume_role_policy = jsonencode({
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
    Version = "2012-10-17"
  })
}


resource "aws_iam_policy" "autoscaler" {
  name   = "ed-eks-autoscaler-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeAutoScalingInstances",
        "autoscaling:DescribeTags",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:SetDesiredCapacity",
        "autoscaling:TerminateInstanceInAutoScalingGroup",
        "ec2:DescribeLaunchTemplateVersions"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_role_policy_attachment" "AmazonSSMManagedInstanceCore" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_role_policy_attachment" "x-ray" {
  policy_arn = "arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess"
  role       = aws_iam_role.worker_node_policy.name
}
resource "aws_iam_role_policy_attachment" "s3" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_role_policy_attachment" "autoscaler" {
  policy_arn = aws_iam_policy.autoscaler.arn
  role       = aws_iam_role.worker_node_policy.name
}

resource "aws_iam_instance_profile" "worker" {
  depends_on = [aws_iam_role.worker_node_policy]
  name       = "quandoo-eks-worker-new-profile"
  role       = aws_iam_role.worker_node_policy.name
}


#
#CREATION OF EKS CLUSTER 
#

resource "aws_eks_cluster" "quandoo_cluster" {
  name     = var.cluster_name
  role_arn = aws_iam_role.quandoo_cluster_policy.arn


  vpc_config {
    subnet_ids = var.private_subnet_ids
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSServicePolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]

}

resource "aws_launch_template" "worker" {
  name     = var.cluster_name
  key_name = var.ec2_key_name

  vpc_security_group_ids = var.vpc_security_group_ids

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${var.cluster_name}-instance"
    }
  }
}


resource "aws_eks_node_group" "worker_node" {
  cluster_name    = aws_eks_cluster.quandoo_cluster.name
  node_group_name = "${var.cluster_name}-node-group"
  node_role_arn   = aws_iam_role.worker_node_policy.arn
  instance_types  = var.worker_node_instance_types
  subnet_ids = var.private_subnet_ids


  launch_template {
    id      = aws_launch_template.worker.id
    version = aws_launch_template.worker.latest_version
  }

  scaling_config {
    desired_size = var.k8s_desired_size
    min_size     = var.k8s_min_size
    max_size     = var.k8s_max_size
  }


  # Allow external changes without causing plan diffs
  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    # aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,

  ]
}

resource "aws_key_pair" "worker_nodes" {
  key_name = var.ec2_key_name
  public_key = file(var.ec2_key)
}

#Secure way to access AWS services from kubernetes RBAC system is using Open ID connect provider
data "tls_certificate" "quandoo" {
  url = aws_eks_cluster.quandoo_cluster.identity[0].oidc[0].issuer
}

resource "aws_iam_openid_connect_provider" "quandoo" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = data.tls_certificate.quandoo.certificates[*].sha1_fingerprint
  url             = data.tls_certificate.quandoo.url
}