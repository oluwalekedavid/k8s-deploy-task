#vpc created to launch our resources
resource "aws_vpc" "main" {
  cidr_block = var.cidr_block

  #Required for EKS. Enable/Disable DNS support in the VPC.
  enable_dns_support = true

  #Required for EKS. Enable/Disable DNS hostnames in the VPC.
  enable_dns_hostnames = true

  tags = {
    Name = "Quandoo-VPC"
  }
}

#data sources used to automatically get the availability zones in us-east-1 instead of declaring it manually. 
data "aws_availability_zones" "us_east_1" {
  state = "available"
}

#internet gateway is used to allow the vpc have access to the internet
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Quandoo-igw"
  }
}

resource "aws_subnet" "public_1" {
  vpc_id = aws_vpc.main.id
  cidr_block = var.public-1-cidr_block
  availability_zone = data.aws_availability_zones.us_east_1.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-subnet-1"
    "kubernetes.io/role/elb"          = "1"
    "kubernetes.io/cluster/quandoo_cluster" = "shared"
  
  }
}

resource "aws_subnet" "public_2" {
  vpc_id = aws_vpc.main.id
  cidr_block = var.public-2-cidr_block
  availability_zone = data.aws_availability_zones.us_east_1.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-subnet-2"
    "kubernetes.io/role/elb"          = "1"
    "kubernetes.io/cluster/quandoo_cluster" = "shared"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id = aws_vpc.main.id
  cidr_block = var.private-1-cidr_block
  availability_zone = data.aws_availability_zones.us_east_1.names[0]
   map_public_ip_on_launch = false

  tags = {
    Name = "Private-subnet-1"
    "kubernetes.io/role/internal-elb"          = "1"
    "kubernetes.io/cluster/quandoo_cluster" = "shared"
  }
}

resource "aws_subnet" "private_2" {
  vpc_id = aws_vpc.main.id
  cidr_block = var.private-2-cidr_block
  availability_zone = data.aws_availability_zones.us_east_1.names[1]
   map_public_ip_on_launch = false

  tags = {
    Name = "Private-subnet-2"
    "kubernetes.io/role/internal-elb"          = "1"
    "kubernetes.io/cluster/quandoo_cluster" = "shared"
  }
}  

resource "aws_subnet" "database_1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.database_cidr_block_1
  availability_zone = data.aws_availability_zones.us_east_1.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name = "Database-subnet"
  }
}

resource "aws_subnet" "database_2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.database_cidr_block_2
  availability_zone = data.aws_availability_zones.us_east_1.names[1]
  map_public_ip_on_launch = false

  tags = {
    Name = "Database-subnet"
  }
}


resource "aws_route_table" "public_route" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Public-Route-Table"
  }
}

resource "aws_route_table_association" "public-a" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.public_route.id
}

resource "aws_route_table_association" "public-b" {
  subnet_id      = aws_subnet.public_2.id
  route_table_id = aws_route_table.public_route.id
}

resource "aws_eip" "nat_gateway_eip" {
  vpc = true
  depends_on = [aws_internet_gateway.igw]

  tags = {
    Name = "quandoo-eip"
  }
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_gateway_eip.id
  subnet_id     = aws_subnet.public_1.id
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = "quandoo-nat-gateway"
  }
}

resource "aws_route_table" "private_route" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "private-route-table"
  }
}

resource "aws_route_table_association" "private-a" {
  subnet_id      = aws_subnet.private_1.id
  route_table_id = aws_route_table.private_route.id
}

resource "aws_route_table_association" "private-b" {
  subnet_id      = aws_subnet.private_2.id
  route_table_id = aws_route_table.private_route.id
}

resource "aws_route_table_association" "database_1" {
  subnet_id      = aws_subnet.database_1.id
  route_table_id = aws_route_table.private_route.id
}

resource "aws_route_table_association" "database_2" {
  subnet_id      = aws_subnet.database_2.id
  route_table_id = aws_route_table.private_route.id
}

resource "aws_security_group" "quandoo-sg" {
  name        = "reelcruit-sg"
  description = "Allows defined inbound traffic for this webservers"
  vpc_id      = aws_vpc.main.id

  dynamic "ingress" {
    for_each = toset(local.ports_in)
    content {
      description = ""
      from_port   = ingress.value
      to_port     = ingress.value
      protocol          = "tcp"
      cidr_blocks       = ["0.0.0.0/0"]
    }
  }
  
  dynamic "egress" {
    for_each = toset(local.ports_out)
    content {
      description = ""
      from_port   = egress.value
      to_port     = egress.value
      protocol          = "-1"
      cidr_blocks       = ["0.0.0.0/0"]
    }
  }

  tags = { Name = "quandoo-security-group" }
}






