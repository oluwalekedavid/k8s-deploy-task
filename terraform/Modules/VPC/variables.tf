variable "cidr_block" {
  description = "cidr block for the VPC"
  default = "10.0.0.0/16"
}

variable "public-1-cidr_block" {
  default = "10.0.1.0/24"
}

variable "public-2-cidr_block" {
  default = "10.0.2.0/24"
}

variable "private-1-cidr_block" {
  default = "10.0.10.0/24"
}

variable "private-2-cidr_block" {
  default = "10.0.11.0/24"
}

variable "database_cidr_block_1" {
  default = "10.0.12.0/24"
}

variable "database_cidr_block_2" {
  default = "10.0.13.0/24"
}


locals {
  ports_in  = [22, 80, 3000, 8080, 443]
  ports_out = [0]
}
