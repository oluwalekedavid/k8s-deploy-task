module "vpc" {
  source = "./Modules/VPC"
  
}

module "eks" {
  source = "./Modules/EKS"
  vpc_id = module.vpc.vpc_id
  public_subnet_ids = module.vpc.public_subnet_ids
  private_subnet_ids = module.vpc.private_subnet_ids
  vpc_security_group_ids = [module.vpc.security_group_info]
}

